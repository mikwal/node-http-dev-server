let ws = require('ws');

module.exports = { runWebSocketProxy };


const CLOSE_NORMAL = 1000;

function runWebSocketProxy(childProcess, liveReloadServer) {

    let wsServer = new ws.Server({ noServer: true });
    let isOpen = ({ readyState }) => readyState === ws.OPEN;

    wsServer.on('connection', (wsClient, request) => {
        
        let onClientError = e => {
            log.error('WebSocket client connection error', e);
            isOpen(wsClient) && wsClient.close(CLOSE_NORMAL);
        };

        childProcess.waitForSocket().then(
            socketPath => {
                let waitForProxiedClient =
                    new Promise((resolve, reject) => {
                        new ws.WebSocket(`ws+unix://${socketPath}:${request.url}`).
                            once('open', function () { 
                                resolve(this);
                                this.removeAllListeners();
                            }).
                            once('error', reject)
                    });

                waitForProxiedClient.then(
                    proxiedClient => {
                        wsClient.once('close', () => isOpen(proxiedClient) && proxiedClient.close(CLOSE_NORMAL));
                        proxiedClient.on('message', (data, binary) => isOpen(wsClient) && wsClient.send(data, { binary }));
                        proxiedClient.once('close', () => isOpen(wsClient) && wsClient.close(CLOSE_NORMAL));
                        proxiedClient.once('error', e => {
                            log.error('WebSocket proxy connection error', e);
                            isOpen(proxiedClient) && proxiedClient.close(CLOSE_NORMAL);
                        });
                    },
                    e => {
                        log.error('Failed to proxy WebSocket connection to server', e);
                        isOpen(wsClient) && wsClient.close(CLOSE_NORMAL);
                    });

                wsClient.on('message', (data, binary) =>
                    waitForProxiedClient.then(
                        proxiedClient => isOpen(proxiedClient) && proxiedClient.send(data, { binary }), 
                        () => { }));
            },
            onClientError)
    });

    wsServer.on('error', e => log.error('WebSocket server error', e));

    return { stop, handleUpgrade };

    function stop() {
        wsServer && wsServer.close();
        wsServer = null;
    }

    function handleUpgrade(request, socket, head) {
        if (liveReloadServer.handleWebSocketUpgrade(request, socket, head)) {
            return;
        }
        wsServer.handleUpgrade(request, socket, head, wsClient => wsServer.emit('connection', wsClient, request));
    }
}
