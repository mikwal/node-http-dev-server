let chokidar = require('chokidar');

module.exports = { watchFileChanges };


function watchFileChanges(callback, options) {

    let include = (options && options.include) || ['.'];
    let exclude = (options && options.exclude) || [];

    options && options.log && options.log.info(
        `Watching files/directories:\n  ${include.join('\n  ')}` +
        (exclude.length > 0 ? `\nIgnoring:\n  ${exclude.join('\n  ')}` : ''));

    chokidar.
        watch(include, {
            ignored: exclude,
            ignoreInitial: true,
            awaitWriteFinish: true
        }).
        on('all', (...args) => {
            switch (args[0]) {
                case 'add':
                case 'change':
                case 'unlink':
                    callback(...args);
                    break;
            }
        });
}