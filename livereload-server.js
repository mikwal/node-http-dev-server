let fs = require('fs');
let url = require('url');
let ws = require('ws');
let log = require('@mikwal/node-console-logger').createLogger('http-dev-server/livereload-server');
let { watchFileChanges } = require('./watch');

module.exports = { runLiveReloadServer };


function runLiveReloadServer(options = {}) {

    let wsServer =
        new ws.Server({ noServer: true }).
            on('connection', sendHello).
            on('error', logError);

    let clientFilePath = (options.liveReload && options.liveReload.path) || '/livereload.js';
    let clientSocketPath = clientFilePath.replace(/\.js$/, '');

    watchFileChanges(reload, {
        include: (options.liveReload && options.liveReload.include) || [...Object.keys(options.assets || {})],
        exclude: (options.liveReload && options.liveReload.exclude) || [],
        log
    });
    
    return { stop, reload, handleGetClientFile, handleWebSocketUpgrade };

    function stop() {
        wsServer && wsServer.close();
        wsServer = null;
    }

    function reload() {
        wsServer && wsServer.clients.forEach(
            sendMessage({
                command: 'reload',
                path: '/',
            })
        );
    }

    function sendHello(socket) {
        sendMessage({
            command: 'hello',
            protocols: ['http://livereload.com/protocols/official-7']
        })(socket);
    }

    function sendMessage(message) {
        message = JSON.stringify(message);
        return socket => void socket.send(message);
    }

    function handleGetClientFile(request, response) {
        switch (`${request.method} ${url.parse(request.url).pathname}`) {
            case `GET ${clientFilePath}`:
                response.writeHead(200, { 'Content-Type': 'text/javascript' });
                fs.createReadStream(__dirname + '/assets/livereload.js').pipe(response);
                return true;
            default:
                return false;
        }
    }

    function handleWebSocketUpgrade(request, socket, head) {
        switch (`${request.method} ${url.parse(request.url).pathname}`) {
            case `GET ${clientSocketPath}`:
                wsServer.handleUpgrade(request, socket, head, wsClient => wsServer.emit('connection', wsClient, request));
                return true;
            default:
                return false;
        }
    }

    function logError(error) {
        log.error('Error running LiveReload server', error);
    }
}
