# NodeJS Development HTTP server

Development mode (with automatic reload) http server for NodeJS micro services. 
Designed with to run a micro service based on `node-http-server` but can be used with any http server capable of listening on a UNIX socket specified by the `PORT` environment variable.  

Restarts the micro service on file changes like `nodemon` but keeps listening for requests whilst restarting for _"Live Reload"_ friendly behaviour.