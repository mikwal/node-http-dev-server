#!/usr/bin/env node

let { runDevelopmentHttpServer } = require('./index.js');

let args = process.argv.slice(2);

if (args.length === 1 && args[0] === '--help') { 
    console.log("usage: http-dev-server [options] [script] [...args]");
    process.exit(2);
}

runDevelopmentHttpServer(args);