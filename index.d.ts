import { Server } from 'http';

export type HttpDevServerOptions = {
    port?: number | string;
    watch?: {
        include?: string | string[];
        exclude?: string | string[];
    },
    liveReload?: {
        path?: string;
        include?: string | string[];
        exclude?: string | string[];
    }
};

export declare function runHttpDevelopmentServer(scriptAndArgs: string[], options?: HttpDevServerOptions): Server;