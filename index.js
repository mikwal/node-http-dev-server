let path = require('path');
let http = require('http');
let log = require('@mikwal/node-console-logger').createLogger('http-dev-server');
let { runHttpServer, loadHttpServerOptions } = require('@mikwal/node-http-server');
let { runChildProcess } = require('./child-process');
let { runLiveReloadServer } = require('./livereload-server');
let { runWebSocketProxy } = require('./websocket-proxy');
let { watchFileChanges } = require('./watch');

module.exports = { runDevelopmentHttpServer };


function runDevelopmentHttpServer(scriptAndArgs, options = (loadHttpServerOptions() || {})) {

    let childProcess = runChildProcess(scriptAndArgs);
    let liveReloadServer = runLiveReloadServer(options);
    let webSocketProxy = runWebSocketProxy(childProcess, liveReloadServer)

    watchFileChanges(childProcess.restart, {
        include: (options.watch && options.watch.include) || [path.dirname(childProcess.scriptPath), ...Object.keys(options.assets || {})],
        exclude: (options.watch && options.watch.exclude) || ['**/node_modules/**/*'],
        log 
    });

    process.on('SIGINT', () => {
        childProcess.stop();
        liveReloadServer.stop();
        webSocketProxy.stop();
    });
    
    return runHttpServer(
        (request, response) => {

            if (liveReloadServer.handleGetClientFile(request, response)) {
                return;
            }

            let onError = error => {
                if (!response.headersSent) {
                    response.statusCode = 500;
                    response.setHeader('Content-Type', 'text/plain');
                    response.end(`${error}`);
                }
            };

            let forwardToChild = socketPath => {

                let { method, headers, url: path } = request;
                let childRequest = http.
                    request({ socketPath, method, path, headers }, childResponse => {
                        response.writeHead(childResponse.statusCode, childResponse.headers);
                        childResponse.pipe(response);
                    }).
                    on('error', onError);
            
                request.pipe(childRequest);
            };

            childProcess.waitForSocket().then(forwardToChild, onError);
        },
        { port: options.port }).
        on('upgrade', webSocketProxy.handleUpgrade);
}

