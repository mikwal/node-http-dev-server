let fs = require('fs');
let crypto = require('crypto');
let log = require('@mikwal/node-console-logger').createLogger('http-dev-server/child-process');
let { spawn } = require('child_process');

module.exports = { runChildProcess };

const POLL_INTERVAL_IN_MS = 500;
const DELAY_START_IN_MS = 500;
const RESTART_ON_ERROR_INTERVAL_IN_MS = 10000;


function runChildProcess(scriptAndArgs) {

    if (!Array.isArray(scriptAndArgs) || !scriptAndArgs.every(s => typeof s === 'string')) {
        throw new Error(`Parameter 'scriptAndArgs' must be an array of a strings`);
    }

    let currentSocketPath = uniqueSocketPath();
    let currentChildProcess = null;
    let scriptPath = scriptAndArgs.find(s => !s.startsWith('-'));

    if (!scriptPath) {
        scriptPath = require.resolve('@mikwal/node-http-server/main');
        scriptAndArgs = [...scriptAndArgs, scriptPath];
    }

    start();

    return { restart, stop, waitForSocket, scriptPath };
    
    function restart() {
        stop();
        start();
    }

    function start() {
        let socketPath = currentSocketPath;
        let pollScriptExists = () => fs.access(scriptPath, fs.constants.R_OK, error => {
            if (socketPath !== currentSocketPath || currentChildProcess) {
                return;
            }
            if (!error) {
                let childProcess = spawn(process.argv[0], scriptAndArgs, { env: { ...process.env, 'PORT': socketPath } }).
                    on('error', error => {
                        log.error(`Error starting child process ${childProcess.pid}`, error);
                        process.stdin.unpipe(childProcess.stdin);
                        currentChildProcess = null;
                        setTimeout(pollScriptExists, RESTART_ON_ERROR_INTERVAL_IN_MS);
                    }).
                    on('exit', errorCode => {
                        log.debug(`Child process ${childProcess.pid} exited with code ${errorCode}`);
                        process.stdin.unpipe(childProcess.stdin);
                        currentChildProcess = null;
                        setTimeout(pollScriptExists, RESTART_ON_ERROR_INTERVAL_IN_MS);
                    });
                log.debug(`Starting child process ${childProcess.pid}`);
                childProcess.stdout.pipe(process.stdout);
                childProcess.stderr.pipe(process.stderr);
                process.stdin.pipe(childProcess.stdin);
                currentChildProcess = childProcess;
                return;
            }
            setTimeout(pollScriptExists, POLL_INTERVAL_IN_MS);
        });
        setTimeout(pollScriptExists, DELAY_START_IN_MS);
    }




    function stop() {
        currentSocketPath = uniqueSocketPath();
        if (currentChildProcess) {
            log.debug(`Killing child process ${currentChildProcess.pid}`);
            currentChildProcess.kill('SIGINT');
            currentChildProcess = null;
        }
    }

    function waitForSocket() {
        let pollSocketExists = resolve => fs.exists(currentSocketPath, exists => {
            if (exists) {
                resolve(currentSocketPath);
                return;
            }   
            setTimeout(pollSocketExists, POLL_INTERVAL_IN_MS, resolve);
        });
        return new Promise(pollSocketExists);
    }
}

function uniqueSocketPath() {
    return `/tmp/${crypto.randomBytes(16).toString('hex')}`;
}